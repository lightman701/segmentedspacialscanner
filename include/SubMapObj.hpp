#include <sl/Camera.hpp>
#include <GL/glew.h>
#include <vector>

class SubMapObj {
    GLuint vaoID_;
    GLuint vboID_[2];
    int current_fc;
    bool need_update;
    bool isUpdating;

    std::vector<sl::float3> vert;
    std::vector<sl::uint3> tri;
    std::vector<sl::uint1> index;

public:
    bool display_mesh;
    SubMapObj();
    ~SubMapObj();
    template<typename T>
    void update(T &chunks);
    void pushToGPU();
    void draw();
};

