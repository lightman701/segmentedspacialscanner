#ifndef __APPLICATION_MODE_H__
#define __APPLICATION_MODE_H__

enum ApplicationMode {
  Idle,
  MapEnvironment,
  DisplayChunks,
  SaveMesh,
	NextChunk,
	PrevChunk
};

#endif
