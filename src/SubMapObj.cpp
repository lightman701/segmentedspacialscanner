#include "SubMapObj.hpp"
#include "Shader.hpp"

SubMapObj::SubMapObj() {
    current_fc = 0;
    vaoID_ = 0;
    need_update = false;
    isUpdating = false;
    display_mesh = false;
}

SubMapObj::~SubMapObj() {
    need_update = false;
    current_fc = 0;
    vert.clear();
    tri.clear();
    if(vaoID_) {
        glDeleteBuffers(2, vboID_);
        glDeleteVertexArrays(1, &vaoID_);
    }
}

template <>
void SubMapObj::update(sl::Chunk &chunk) {
    if(!need_update) {
        vert = chunk.vertices;
        tri = chunk.triangles;
        need_update = true;
        display_mesh = true;
    }
}

template <>
void SubMapObj::update(sl::PointCloudChunk &chunk) {
    if(!need_update) {
	int nb_v = chunk.vertices.size();
        vert.resize(nb_v);
        index.resize(nb_v);
        for(int c = 0; c < nb_v; c++){
	    index[c] = c;
	    vert[c] = chunk.vertices[c];
	}
        need_update = true;
        display_mesh = false;
    }
}

void SubMapObj::pushToGPU() {
    if(need_update) {
        isUpdating = true;
        if(vaoID_ == 0) {
            glGenVertexArrays(1, &vaoID_);
            glGenBuffers(2, vboID_);
        }

        glBindVertexArray(vaoID_);

        glBindBuffer(GL_ARRAY_BUFFER, vboID_[0]);
        glBufferData(GL_ARRAY_BUFFER, vert.size() * sizeof(sl::float3), &vert[0], GL_DYNAMIC_DRAW);
        glVertexAttribPointer(Shader::ATTRIB_VERTICES_POS, 3, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(Shader::ATTRIB_VERTICES_POS);
        if(display_mesh) {
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboID_[1]);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, tri.size() * sizeof(sl::uint3), &tri[0], GL_DYNAMIC_DRAW);
            current_fc = (int) tri.size() * 3;
        } else {
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboID_[1]);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, index.size() * sizeof(sl::uint1), &index[0], GL_DYNAMIC_DRAW);
            current_fc = (int) index.size();
        }

        glBindVertexArray(0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        need_update = false;

        isUpdating = false;
    }
}

void SubMapObj::draw() {
    if(current_fc && vaoID_ && !isUpdating) {
        glBindVertexArray(vaoID_);
        glDrawElements(display_mesh ? GL_TRIANGLES : GL_POINTS, (GLsizei) current_fc, GL_UNSIGNED_INT, 0);
        glBindVertexArray(0);
    }
}

